console.log("Hello World!");

let numEntered = Number(prompt("Please enter a number:"));

console.log("The number you provided is " + numEntered + ".");


for( let i = numEntered; i >= 0; numEntered-- ){
    if(numEntered % 10 === 0){
    console.log("The number is divisible by 10. Skipping the number!");
    continue;
    }
    if(numEntered % 5 === 0 ){
    console.log(numEntered);
    continue;
    }
    if(numEntered <=50 ){
    console.log("The current value is at 50. Terminating the loop.")
    break;
    }
}


let supString = "supercalifragilisticexpialidocious"
let vowels = ['a', 'e', 'i', 'o', 'u'];
let consonants = ['s', 'p', 'r', 'c','l', 'f', 'g','s', 't', 'x', 'd'];
let gap = ""

console.log(supString)

for(let p = 0; p < supString.length; p++){
	if(supString[p] === 'a' ){
		continue;
	}
	if(supString[p] === 'e' ){
		continue;
	}
	if(supString[p] === 'i' ){
		continue;
	}
	if(supString[p] === 'o' ){
		continue;
	}
	if(supString[p] === 'u' ){
		continue;
	}
	else {
		gap += supString[p];
		
	}
}
console.log(gap);

// Shortcut		
/*for(let p = 0; p < supString.length; p++)
    {        
        if (!vowels.includes(supString[p]))
        {
            gap += supString[p];
        }
    }
    console.log(gap);*/